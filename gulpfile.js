var gulp = require('gulp');
var $ = require('gulp-load-plugins')({lazy: true});
var config = require('./gulp.config')();
var path = require('path');
var _ = require('lodash');
var webpack = require('webpack');
var webpackConfig = require("./webpack.config.js");
var del = require('del');
//Karma server
// var Server = require('karma').Server;

//Check if the require didnt work and we require all the gulps manual
if(!$.connect){
    // $.autoprefixer = require("gulp-autoprefixer");
    $.concat = require("gulp-concat");
    $.connect = require("gulp-connect");
    $.csso = require("gulp-csso");
    $.inject = require("gulp-inject");
    $.open = require("gulp-open");
    $.plumber =require("gulp-plumber");
    $.rename = require("gulp-rename");
    // $.replace = require("gulp-replace");
    $.sourcemaps = require("gulp-sourcemaps");
    $.uglify = require("gulp-uglify");
    $.util = require("gulp-util");
    $.watch = require("gulp-watch");
}

//----------------------------------------------------------------------------------------------------------------------
// Local server tasks
//----------------------------------------------------------------------------------------------------------------------

gulp.task('connect', ['dev'], function () {
    return $.connect.server({
        root: [config.app],
        port: config.port,
        base: config.devBaseUrl
    })
});

gulp.task('open', ['connect'], function (done) {
    return gulp.src(config.index)
        .pipe($.open({
            uri: config.devBaseUrl + ':' + config.port + '/'
        }), done())
});

gulp.task('connect-production', ['dist'], function () {
    return $.connect.server({
        root: [config.build],
        port: config.port,
        base: config.devBaseUrl
    })
});

gulp.task('open-production', ['connect-production'], function (done) {
    return gulp.src(config.indexProd)
        .pipe($.open({
            uri: config.devBaseUrl + ':' + config.port + '/'
        }), done())
});

//----------------------------------------------------------------------------------------------------------------------
// Clean code tasks
//----------------------------------------------------------------------------------------------------------------------

gulp.task('clean-dev', function () {
    var files = [].concat(
        config.temp + '**/*'
    );
    clean(files);
});

gulp.task('clean-dist', function () {
    var files = [].concat(
        config.build + '**/*'
    );
    clean(files);
});

//----------------------------------------------------------------------------------------------------------------------
// Images and fonts tasks
//----------------------------------------------------------------------------------------------------------------------

// gulp.task('font-images', ['optimizeJs'], function () {
//     log('Moving images and fonts from temp to dist/styles');

//     return gulp.src([
//             config.temp + '*.ttf',
//             config.temp + '*.png'
//         ])
//         .pipe(gulp.dest(config.build + '/styles/'))
// });


gulp.task('bootstrapFonts', ['fontAwesomeFonts'], function () {
    log('Copying bootstrap fonts');

    return gulp.src(config.bootstrapFonts)
        .pipe(gulp.dest(config.build + 'fonts'));
});

gulp.task('fontAwesomeFonts', function () {
    return gulp.src(config.fontAwesomeFonts)
        .pipe(gulp.dest(config.build + 'fonts'));
});

//----------------------------------------------------------------------------------------------------------------------
// Webpack task
//----------------------------------------------------------------------------------------------------------------------

gulp.task('webpack', ['clean-dev'], function (callback) {
    var devConfig = Object.create(webpackConfig);
    devConfig.devtool = "sourcemap";
    devConfig.debug = true;

    webpack(devConfig, function (err, stats) {
        if (err) throw new gutil.PluginError("webpack", err);
        $.util.log("[webpack]", stats.toString({
            colors: true,
            chunks: false,
            children: false
        }));
        callback();
    });
});

//----------------------------------------------------------------------------------------------------------------------
// Develop task
//----------------------------------------------------------------------------------------------------------------------

gulp.task('dev', function () {
    log('injecting css and scripts into the html');
    return gulp.src(config.indexSeed)
        .pipe($.rename(config.index))
        .pipe($.plumber())
        .pipe($.inject(gulp.src(config.libsCss), {
            addRootSlash: false,
            ignorePath: '/src',
            starttag: '<!-- inject:libs:css -->'
        }))
        .pipe($.inject(gulp.src(config.temp + 'app.css'), {
            addRootSlash: false,
            ignorePath: '/src'
        }))
        .pipe($.inject(gulp.src(config.libsJs), {
            addRootSlash: false,
            ignorePath: '/src',
            starttag: '<!-- inject:libs:js -->'
        }))
        .pipe($.inject(gulp.src([config.temp + 'app.js']), {
            addRootSlash: false,
            ignorePath: '/src'
        }))
        .pipe(gulp.dest('./'));
});

//----------------------------------------------------------------------------------------------------------------------
// Minification tasks
//----------------------------------------------------------------------------------------------------------------------

gulp.task('libStyles', function () {

    return gulp.src(config.libsCss)
        .pipe($.sourcemaps.init())
        .pipe($.concat('libs.css'))
        .pipe($.csso())
        .pipe($.sourcemaps.write('./'))
        .pipe(gulp.dest(config.build + '/styles'))
});

gulp.task('optimizeCss', ['webpack'], function () {

    return gulp.src(config.temp + 'app.css')
        .pipe($.sourcemaps.init({loadMaps: true}))
        .pipe($.csso())
        .pipe($.sourcemaps.write('./'))
        .pipe(gulp.dest(config.build + '/styles'))
});

gulp.task('libScripts', function () {

    return gulp.src(config.libsJs)
        .pipe($.plumber())
        .pipe($.sourcemaps.init())
        .pipe($.concat('libs.js'))
        .pipe($.uglify({
            compress: {
                drop_console: true
            }
        }))
        .pipe($.sourcemaps.write('./'))
        .pipe(gulp.dest(config.build + '/scripts'))
});

gulp.task('optimizeJs', ['optimizeCss'], function () {

    return gulp.src(config.temp + 'app.js')
        .pipe($.sourcemaps.init({loadMaps: true}))
        .pipe($.uglify({
            compress: {
                drop_console: true
            }
        }))
        .pipe($.sourcemaps.write('./'))
        .pipe(gulp.dest(config.build + '/scripts'))
});

//----------------------------------------------------------------------------------------------------------------------
// Production task
//----------------------------------------------------------------------------------------------------------------------

gulp.task('dist', ['clean-dist', 'bootstrapFonts', 'libStyles', 'libScripts', 'optimizeJs'], function () {
    log('injecting minified css and scripts into the html');

    return gulp.src(config.index)
        .pipe($.plumber())
        .pipe($.inject(
            gulp.src(config.build + 'styles/libs.css'), {
                ignorePath: '/dist',
                starttag: '<!-- inject:libs:css -->'
            }))
        .pipe($.inject(
            gulp.src(config.build + 'styles/app.css'), {
                ignorePath: '/dist'
            }))
        .pipe($.inject(
            gulp.src(config.build + 'scripts/libs.js'), {
                ignorePath: '/dist',
                starttag: '<!-- inject:libs:js -->'
            }))
        .pipe($.inject(
            gulp.src([
                config.build + 'scripts/app.js'
            ]), {
                ignorePath: '/dist'
            }))
        .pipe(gulp.dest(config.build));
});

//----------------------------------------------------------------------------------------------------------------------
// Unit tests tasks
//----------------------------------------------------------------------------------------------------------------------

// gulp.task('test', function (done) {
//     new Server({
//         configFile: __dirname + '\\karma.config.js',
//         singleRun: true
//     }, done).start();
// });

// gulp.task('ctest',  function (done) {
//     new Server({
//         configFile: __dirname + '\\karma.config.js',
//         singleRun: false
//     }, done).start();
// });

//----------------------------------------------------------------------------------------------------------------------
// Default tasks for stating gulp
//----------------------------------------------------------------------------------------------------------------------


gulp.task('serve-build', ['open-production'], function () {
    webpackWatch();

    var msg = {
        title: 'gulp build',
        subtitle: 'Deployed to the build folder',
        message: 'Running `gulp serve-build`'
    };
    notify(msg);
});

gulp.task('serve-dev', ['open'], function () {
    webpackWatch();

    var msg = {
        title: 'gulp dev',
        subtitle: 'Deployed to the app folder',
        message: 'Running `gulp serve-dev`'
    };
    notify(msg);
});

gulp.task('default', ['serve-dev'], function () {
    webpackWatch();
});

/////////////////////////////////////////////////////////////

function webpackWatch() {
    var devConfig = Object.create(webpackConfig);
    devConfig.devtool = "sourcemap";
    devConfig.debug = true;
    devConfig.watch = true;

    webpack(devConfig, function (err, stats) {
        if (err) throw new gutil.PluginError("webpack", err);
        $.util.log("[webpack]", stats.toString({
            colors: true,
            chunks: false,
            children: false
        }));
    });
}

function log(msg) {
    if (typeof (msg) === 'object') {
        for (var item in msg) {
            if (msg.hasOwnProperty(item)) {
                $.util.log($.util.colors.blue(msg[item]));
            }
        }
    } else {
        $.util.log($.util.colors.blue(msg));
    }
}

function notify(options) {
    var notifier = require('node-notifier');
    var notifyOptions = {
        sound: 'Bottle',
        contentImage: path.join(__dirname, 'gulp.png'),
        icon: path.join(__dirname, 'gulp.png')
    };
    _.assign(notifyOptions, options);
    notifier.notify(notifyOptions);
}

function clean(path) {
    log('Cleaning: ' + $.util.colors.blue(path));
    del.sync(path, {force: true});
}