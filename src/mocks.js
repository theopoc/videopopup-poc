// Mock data
export default [
	{
		id:'6JQm5aSjX6g',
		name: 'Video 1',
		comments: [
			{
				author: 'Stoyan Daskalov',
				date: new Date(),
				avatar: '',
				comment:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut aliquam, ligula in eleifend maximus, enim eros mattis nisi, eget dapibus metus erat nec nisi. Sed egestas tempor turpis et congue. Morbi malesuada est et laoreet iaculis. Fusce pellentesque tortor lacus, id venenatis nisl aliquam sit amet. Suspendisse at est sapien. Aliquam lobortis dolor leo',
			},
			{
				author: 'Stoyan Daskalov',
				date: new Date(),
				avatar: '',
				comment:'itae suscipit lorem malesuada in. Fusce vitae iaculis nisi.'
			},
			{
				author: 'Stoyan Daskalov',
				date: new Date(),
				avatar: '',
				comment:'venenatis odio, posuere tristique lectus metus et leo. Vestibulum mollis vulputate mi, vel tincidunt urna iaculis non. Pellentesque imperdiet neque sed ante euismod, sit amet pulvinar libero vulputate. Nunc diam eros, tempus et sagittis eu, elementum sed velit. Cras tincidunt eget tellus a commodo.'
			}
		]
	},
	{
		id:'smoEelV6FUk',
		name: 'Video 2',
		comments: [{
				author: 'Teodor Todorov',
				date: new Date(),
				avatar: '',
				comment:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut aliquam, ligula in eleifend maximus, enim eros mattis nisi, eget dapibus metus erat nec nisi. Sed egestas tempor turpis et congue. Morbi malesuada est et laoreet iaculis. Fusce pellentesque tortor lacus, id venenatis nisl aliquam sit amet. Suspendisse at est sapien. Aliquam lobortis dolor leo',
			},]
	},
	{
		id:'izm8diKEQlw',
		name: 'Video 3',
		comments: []
	}
];