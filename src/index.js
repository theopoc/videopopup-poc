// Bootstraps the application - an entry point for the webpack.

require('./skin.scss');
function requireAll(requireContext) {
    return requireContext.keys().map(requireContext);
}
var req = require;

//Requires all the comonents in the WebPack - imprves the scalability
requireAll(req.context("./components/", true, /^((?!\.spec).)*\.js$/));

// Uses a mock for demo purposes - the data is usually obtained from a server endpoint
mock = require('./mocks.js').default;

//Fires the knockout app
function GlobalViewModel(videos, selectedVideo) {
    this.videos = videos;
    this.selectedVideo = selectedVideo;
}

ko.applyBindings(new GlobalViewModel(ko.observable(mock), ko.observable({})));