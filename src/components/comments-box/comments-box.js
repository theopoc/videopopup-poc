require("./comments-box.scss");

ko.components.register('comments-box', {
    viewModel: function(params) {
      this.comments = params.comments;

      this.addComment = (form) => {
      	let input = form.comment;

        //quite ugly - defines a "mock" of the undefined values for the comment
      	this.comments().push({
      			author: 'Stoyan Daskalov',
    				date: new Date(),
    				avatar: '',
    				comment:input.value
    			});

      	this.comments.notifySubscribers();
      	input.value = '';
      }
    },
    template: require('./comments-box.html')
});