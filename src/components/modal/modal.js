// A KnockOutJS binding for the bootstrap modal

require('./modal.scss');

ko.bindingHandlers.modal = {
    init: function (element, valueAccessor) {
        $(element).modal({
            show: false
        });
        
        var value = valueAccessor();
        if (typeof value === 'function') {
            $(element).on('hide.bs.modal', function() {
               value({});
            });
        }
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
           $(element).modal("destroy");
        });
        
    },
    update: function (element, valueAccessor) {
        var value = valueAccessor(),
            video = ko.utils.unwrapObservable(value);
        if (!_.isEqual(video, {})) {
            ko.dataFor(element).videoUrl(video.id);
            $(element).modal('show');
        } else {
            $(element).modal('hide');
        }
    }
}

ko.components.register('modal', {
    viewModel: function(params) {
        this.selectedVideo = params.selectedVideo;
        this.videoUrl = ko.observable(this.selectedVideo().id);

        this.closeModal = function () {
            this.selectedVideo({});
        }
    },
    template: require('./modal.html')
});