// KnockoutJS Bindings for the player

require("./player.scss");

var customPlayer = require('./customPlayer.js').customPlayer;
let playbackPosition = ko.observable(0),
    playingStatusIcon = ko.observable(''),
    volumeStatusIcon = ko.observable(''),
    remainingTimeChange = ko.observable('');

ko.components.register('player', {
    viewModel: function(params) {
      this.videoUrl = params.videoUrl;

      this.play = function () { playerInstance.togglePlay() }.bind(this);
      this.volume = function () { playerInstance.volume() }.bind(this);

      //binds with the customPlayer
      this.setSeekValue = function (value) {
        playerInstance.seekAction(value) 
      }.bind(this);

      this.playbackPosition = playbackPosition;
      this.playingStatusIcon = playingStatusIcon;
      this.volumeStatusIcon = volumeStatusIcon;
      this.remainingTimeChange = remainingTimeChange;
    },
    template: require('./player.html')
});

ko.bindingHandlers.player = {
    update: function (element, valueAccessor) {
        let value = valueAccessor(),
            unwrappedValue = value();
        if (unwrappedValue) {
          playerInstance.init(unwrappedValue);
        } else {
          playerInstance.pauseVideo();
        }
    }
}

var playerInstance = new customPlayer();
playerInstance.subscribe('PlaybackPositionkChange', playbackPosition);
playerInstance.subscribe('PlayingStatusIconChange', playingStatusIcon);
playerInstance.subscribe('VolumeStatusIconChange', volumeStatusIcon);
playerInstance.subscribe('RemainingTimeChange', remainingTimeChange);