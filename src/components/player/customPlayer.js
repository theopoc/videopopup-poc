//Custom player that works with the YouTube Player API
//  Implemets the state and the pub/sub patterns
//  Depends on the YouTybe API
//  Assumes the observers will use FontAwesome for the icons - can be tweaked.


    //importing the states for the state pattern
let state = require('./states/abstractState.js').state,
    pausedState = require('./states/pausedState.js').pausedState,
    playingState = require('./states/playingState.js').playingState,

    //private properties
    // the storage for the pub/sub
    observers = {},
    // quasi singleton 
    isInitialized = false,
    // the YouTube video-id currently played 
      // - inited to a blank video Id (cannot instantiate the Player API with no video loaded)
    videoId = '8tPnX7OPo0Q';

export class customPlayer {
  constructor () {
      self = this;
      //the initial state
      self.state = new pausedState();
      //the player - needs to be public to be accessed by the states
      self.player = undefined;
      //timeout id for the elapsed indicator
      self.indicatorTimeout = undefined;
  }

  // initializes the palyer with as a quasi singleton
  init (vId) {
      if (vId) { videoId = vId; }
      if(!isInitialized) {
        isInitialized = true;
        self.player = new YT.Player('player-container', {
            videoId: videoId, 
            events: {
              //when the player is ready, a playback will be started with the passed videoId
              'onReady': self.onPlayerReady
            }
        });
      } else {
        //set the video ID and start playback
        self.setVideoId(vId);
      }
  };


  //Event handlers
  onPlayerStateChange (event) {
    //if the state === 2, pause playback, by setting the appropriate state
    if (event.data === 2) {
      self.pauseVideo();
    } else {
      self.playVideo();
    }
  };

  //sets the default values, the state and notifies the observers about the current state
  onPlayerReady(event) {
    self.playVideo();
    self.player.setVolume(100);
    self.publish('VolumeStatusIconChange', 'fa-volume-up');
    self.player.addEventListener('onStateChange', self.onPlayerStateChange);

    //a bit of a hack  - makes sure the video is visible on old browsers before starting playback
    setTimeout( function () {
      self.setVideoId(videoId);
    }, 100);
  };
  //End event handlers

  //sets the video Id and starts its playback
  setVideoId (vId) {
    videoId = vId;
    self.player.loadVideoById(vId);
  };

  // prepares the UI for playback and starts it
  playVideo() {
    self.setState(new playingState());
    self.stopPlayingIndicator();
    self.startPlayingIndicator();
    self.publish('PlayingStatusIconChange', 'fa-pause');
    if (self.player && typeof self.player.playVideo === 'function') {
      self.player.playVideo();
    }
  };

  // prepares the UI to pause and pauses the player
  pauseVideo() {
    self.setState(new pausedState());
    self.stopPlayingIndicator();
    self.publish('PlayingStatusIconChange', 'fa-play');
    if (self.player && typeof self.player.pauseVideo === 'function') {
      self.player.pauseVideo();
    }
    
  };

  // volume toggler - toggles between mute/50/100 and changes the icon
  volume() { 
    let volume = self.player.getVolume(),
        volumeStatusIcon = 'fa-volume-up';
    if (self.player.isMuted()) {
      self.player.unMute();
      volumeStatusIcon = 'fa-volume-down';
      volume = 50;
    } else {
      if (volume < 50 && volume > 0) {
        volume = 50;
        volumeStatusIcon = 'fa-volume-down';
      } else if (volume < 100 && volume >= 50) {
        volume = 100;
        volumeStatusIcon = 'fa-volume-up';
      } else if (volume === 100) {
        self.player.mute();
        volumeStatusIcon = 'fa-volume-off';
      }
    }
    self.publish('VolumeStatusIconChange', volumeStatusIcon);
    self.player.setVolume(volume);
  };

  // A recursive timeout function to indicate elapsed time and playback slider position
  startPlayingIndicator() {
    if (self.player && typeof self.player.getCurrentTime === 'function') {
      let currentTime = self.player.getCurrentTime(),
        duration = self.player.getDuration(),
        position = 100/(duration/currentTime),

        //utility functions
          // adds a leading zero
        twoDigits = (int) => {
          if (isNaN(int)) {
            int = 0;
          }
          return ('00' + int).slice(-2);
        },
        // formats the time to format [##:]##:##. Accepts a float seconds - elapsed secconds 
        formatSeconds = (seconds) => {
          //return the seconds as default
          let out = seconds;
          //in order to work correctly, int values are needed 
          //  (the leading zero formatter does not support floats)
          seconds = Math.round(seconds);

          // more then one minute and less then an hour - ##:## format desired
          if (seconds > 60 && seconds < 3600) {
            out = twoDigits(Math.round(seconds/60)) + ':' + twoDigits(seconds % 60);
          } else if (seconds > 3600) {
            // more than an hour - ##:##:## format is desired
            let hours = Math.round(seconds/3600),
                newSeconds = seconds % 3600;
            out = twoDigits(hours) + ':' + twoDigits(Math.round(newSeconds/60)) + ':' + twoDigits(newSeconds % 60);
          } else {
            //less than a minute - prepend with zeros 00:##
            out = '00:' + twoDigits(seconds);
          }
          return out;
        };
      // recursively calls itself in one second 
      // - do not want to use an interval to avoid memory leaks on older browsers for longer playbacks
      self.indicatorTimeout = setTimeout(self.startPlayingIndicator, 1000);

      //notifies the observers
      self.publish('PlaybackPositionkChange', position);
      self.publish('RemainingTimeChange', '-' + formatSeconds(duration - currentTime));
    }
    
  };

  //stops the recursion for the indicator and resets the values for the observers
  stopPlayingIndicator() {
    clearTimeout(self.indicatorTimeout);
    self.publish('PlaybackPositionkChange', 0);
    self.publish('RemainingTimeChange', '');
  };

  // Pub/Sub

  // Subscribes an observer for a topic;
  //   Returns a function for un-subscribing
  subscribe (topic, observer) {
    if (typeof observer === 'function') {
      if (!observers[topic]) {
        observers[topic] = [];
      }
      var len = observers[topic].push(observer);
      return function () {
        delete observers[topic][len - 1];
      }
    }
  };
  // Notifies the observers of a topic
  publish (topic, value) {
    if (observers[topic]){
      for (let i=0, a=observers[topic].length; i<a; i++) {
        let observer = observers[topic][i];
        if (typeof observer == 'function') {
          observer(value);
        }
      }
    }
  }
  //End Pub/Sub

  //State pattern
  setState (newState) {
    self.state = newState;
  };

  togglePlay() { return self.state.togglePlay(this)};
  seekAction(percent) { return self.state.seekAction(this, percent)};
  // End state pattern
}