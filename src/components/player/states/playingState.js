// Implements the interface for the state and declares the paused state
var state = require('./abstractState.js').state;

export class playingState extends state {
  togglePlay (context) {
  	var pausedState = require('./pausedState.js').pausedState;
    context.player.pauseVideo();
    context.setState(new pausedState());
    context.stopPlayingIndicator();
    self.publish('PlayingStatusIconChange', 'fa-pause');
  }
  seekAction (context, percent) {
    context.player.seekTo(context.player.getDuration()*(percent/100), true);
  }
}