// Implements the interface for the state and declares the paused state
var state = require('./abstractState.js').state;

export class pausedState extends state {
  togglePlay (context) {
  	var playingState = require('./playingState.js').playingState;
    context.player.playVideo();
    context.setState(new playingState());
    context.startPlayingIndicator();
    self.publish('PlayingStatusIconChange', 'fa-play');
  }
  seekAction (context, percent) {
    context.player.seekTo(context.player.getDuration()*(percent/100), true);
    self.togglePlay();
  }
}