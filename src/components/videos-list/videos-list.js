ko.components.register('videos-list', {
    viewModel: function(params) {
      this.videos = params.videos;
      this.selectedVideo = params.selectedVideo;

      this.openModal = function (video) {
      	this.selectedVideo(video);
      }.bind(this);
    },
    template: require('./videos-list.html')
});