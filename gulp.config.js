module.exports = function () {
    var app = './src/';
    var temp = './src/.tmp/';
    var build = './dist/';

    var config = {
        app: app,
        build: build,
        //integration with .net sln
        devBaseUrl: 'http://localhost',
        index: app + 'index.html',
        indexSeed: app + 'index-seed.html',
        indexProd: build + 'index.html',
        bootstrapFonts: app + 'vendor/bootstrap/fonts/**/*',
        fontAwesomeFonts: app + 'vendor/font-awesome/fonts/*',
        images: app + 'assets/images/**/*',
        libsCss: [
            app + 'vendor/bootstrap/dist/css/bootstrap.css',
            app + 'vendor/font-awesome/css/font-awesome.css',
            app + 'vendor/seiyria-bootstrap-slider/dist/css/bootstrap-slider.min.css',
        ],
        libsJs: [
            app + 'vendor/jquery/dist/jquery.js',
            app + 'vendor/lodash/lodash.js',
            app + 'vendor/bootstrap/dist/js/bootstrap.js',
            app + 'vendor/knockout/dist/knockout.debug.js',
            app + 'vendor/seiyria-bootstrap-slider/dist/bootstrap-slider.js'
        ],
        port: 3001,
        temp: temp
    };

    return config;
};
