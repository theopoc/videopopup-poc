# VideoPlayerModal PoC

A demo KnockOutJS application, that implements a videoPopup, based on a provided PSD layout. 
General design
- It's based on the web component standard. All the components can be found in the src/components directory.
- The application is developed in ES6, which is transpiled by Bable;
- It's based on the CommonJs Pattern, using WebPack;
- The icons are added in a custom font;
- A mock file is used to store some data for the demo (mock.js);
- Two package managers are used - NPM for the dev deps and bower for the deps;
- Gulp is used as a task-runner. The distribution minifes and concats the code of both app and the deps;
- I decided to code a wrapper of the YouTube API and not use a different player or try to "make-up" the YouTube one with CSS, just to demonstrate skills - in practice some simpler approach might be a better choice; 
- The customPlayer.js implements the following design patterns:
- - The State pattern;
- - The Pub/Sub pattern;
- - A slightly modified singleton.

### To run the application:
 - Clone the repo and get install the dependencies by running
 ```sh
        git clone {path});
        npm i && bower i
```
 - To start in dev mode:
 ```sh
        run gulp
 ```
 - To build the application run:
 ```sh
       gulp dist
 ```
 - To run the dist version run:
```sh
       gulp serve-build
```

### Demo
An online demo can be accessed [here](http://a.poveche.info/videoPlayerPoC).

### Configurations
The skin colors can be configured in the assets/_variables.scss file.

### TODO
 - Use [MomentJS] to format the date in comments - did not have the time to do it;
 - Use the font from the PSD file - did not have the time to obtain and include them, but it's evident I can do it, since I can use a custom-made font for the SVG icons;
 - Publish constatnts instead of FA identifiers for the observers from the customPlayer.js;
 - Configure Unit-testing and write some - did not have the time for this, but started it in the gulpfile;
 - Start using some automated doc generator - did not have the time to do it;
 - Check for memory leaks - I fixed one during the development and on the first glance no other leaks can be found in the code, but it worths checking;
 - Write some more comments;
 - Test on different browsers (only Chrome was tested);
 - Make it responsive.

### Dependencies (via bower)
The application depends on:
* [Twitter Bootstrap]
* [FontAwesome]
* [LoDash]
* [jQuery]
* [KnowckoutJS]
* [Bootstrap slider]
* [YouTube iFrame Embed API]

### Version
0.0.1



License
----

MIT