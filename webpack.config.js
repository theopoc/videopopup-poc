var webpack = require("webpack");
var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var autoprefixer = require('autoprefixer');
var precss = require('precss');

module.exports = {
    entry: "./src/index.js",
    output: {
        path: path.join(__dirname, "src/.tmp"),
        filename: "app.js"
    },
    resolve: {
        extensions: ["", ".webpack.js", ".web.js", ".js"]
    },
    plugins: [
        new ExtractTextPlugin("app.css")
    ],

    module: {
        preLoaders: [
           // {
           //      test: /\.js$/, // include .js files
           //      exclude: /node_modules|vendor/, // exclude any and all files in the node_modules folder
           //      loader: "jshint-loader"
           //  }
        ],
        loaders: [
            {
                test: /\.js$/,
                exclude: /(node_modules|vendor)/,
                loader: 'babel', // 'babel-loader' is also a legal name to reference
                query: {
                    presets: ['es2015']
                }
            },
            {
                test: /\.html$/,
                loader: 'html'
            },
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract("style-loader", "css-loader!postcss-loader!sass-loader")
            },
            {
                test: /\.(png|jpg|gif|svg|woff|woff2|eot|ttf)$/,
                loader: "url-loader?&limit=100000"
            }
        ]
    },

    jshint: {
        // any jshint option http://www.jshint.com/docs/options/
        // i. e.
        camelcase: true,

        // jshint errors are displayed by default as warnings
        // set emitErrors to true to display them as errors
        emitErrors: true,

        // jshint to not interrupt the compilation
        // if you want any file with jshint errors to fail
        // set failOnHint to true
        failOnHint: true,
    },


    sassLoader: {
        includePaths: ["./src/assets/", "variables.scss"]
    },
    
    postcss: function () {
        return [require('autoprefixer'), require('precss')];
    }
};
